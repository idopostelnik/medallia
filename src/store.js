import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userName: 'Jim',
    messagesList: [
      {
        subject: 'Hi',
        content: 'Consequat velit laboris tempor nisi.',
        isRead: false
      },
      {
        subject: 'Sports newsletter',
        content: 'Esse minim est ea sit est nisi qui minim pariatur cillum sunt nisi nostrud est.',
        isRead: false
      },
      {
        subject: 'I am not a spam',
        content: 'Eu eiusmod et incididunt magna aute mollit enim dolore reprehenderit excepteur non excepteur minim est.',
        isRead: false
      },
      {
        subject: 'Want to win 1M$?',
        content: 'Culpa incididunt nulla reprehenderit sint cillum duis nulla dolor est mollit aliqua ea dolore.',
        isRead: false
      },
      {
        subject: 'Want to win 2M$?',
        content: 'Lorem adipisicing minim sunt ex culpa nostrud.',
        isRead: false
      },
      {
        subject: 'Hi again',
        content: 'Veniam anim eu sit sit velit deserunt do labore duis commodo adipisicing irure.',
        isRead: false
      },
      {
        subject: 'Nature blog',
        content: 'Occaecat cupidatat non dolore aliquip veniam aliqua ea id excepteur.',
        isRead: false
      },
      {
        subject: 'Click me',
        content: 'Dolore anim aliqua quis tempor.',
        isRead: false
      }
    ]
  },
  getters: {
    unreadMessages: state => {
      let counter = 0
      state.messagesList.forEach((msg) => {
        if (!msg.isRead) counter++
      })
      return counter
    },
    totalMessages: state => state.messagesList.length
  },
  mutations: {
    UPDATE_messageState (state, payload) {
      let index = payload.index
      let isRead = payload.isRead
      state.messagesList[index].isRead = isRead
    }
  },
  actions: {
    setAllAsUnRead (state) {
      state.state.messagesList.forEach((msg, index) => {
        state.commit('UPDATE_messageState', {
          index,
          isRead: false
        })
      })
    }
  },
  plugins: [createPersistedState()]
})
